/*DROP TABLE*/
drop table graffiti;
drop table building;
drop table profile;
drop table calendar;



CREATE TABLE profile(
  email varchar(32) not null,
  password varchar(32) not null,
  first_name varchar(32) not null,
  last_name varchar(32) not null,
  adress varchar(32) not null,
  num varchar(32) not null,
  status int not null,
  PRIMARY KEY(email)
);

create table building(
    id  SERIAL,
    id_client VARCHAR(32),
    name VARCHAR(32) not null,
    adress varchar(32) not null,
    PRIMARY KEY(id),
    FOREIGN KEY(id_client) REFERENCES profile(email) ON DELETE CASCADE
);

create table graffiti(
    id  SERIAL,
    id_building BIGINT(20) UNSIGNED NOT NULL,
    location VARCHAR(32) not null,
    floor int not null,
    status int not null,
    date date,
    description text,
    link_picture text,
    PRIMARY KEY(id),
    FOREIGN KEY(id_building) REFERENCES building(id) ON DELETE CASCADE
);

CREATE TABLE calendar(
    id SERIAL,
    text VARCHAR(32) not null,
    start_date TIMESTAMP not null,
    end_date TIMESTAMP not null,
    PRIMARY KEY(id)
);


/*INSERT INTO TABLE*/
insert into profile(email, password, first_name, last_name, adress, num, status)
values ('ziwiakowsky.paul@gmail.com', 'yolo', 'paul', 'ziwiakowsky', 'Avenue des champs elysées', '761914337', 1);

insert into profile(email, password, first_name, last_name, adress, num, status)
values ('ben@gmail.com', 'yolo', 'lea', 'ben', 'Avenue des champs elysées', '0635', 1);

insert into profile(email, password, first_name, last_name, adress, num, status)
values ('ok@gmail.com', 'yolo', 'lea2', 'ben2', 'Avenue des champs elysées', '0635', 1);

insert into profile(email, password, first_name, last_name, adress, num, status)
values ('bob@bob.bob', 'bob', 'bob', 'bob', 'Avenue des Bob', '282282282', 0);

insert into building(id_client, name, adress)
values ('ziwiakowsky.paul@gmail.com', 'Building 1', '13 rue blabla');

/*insert into graffiti(id_building, location, floor, status, date, description)
values (1, "S", 3, 1, null, null);

insert into graffiti(id_building, location, floor, status, date, description)
values (1, "SE", 1, 1, null, null);

insert into graffiti(id_building, location, floor, status, date, description)
values (1, "N", 1, 1, null, null);*/

insert into calendar(text, start_date, end_date)
values ('Something', '2018-12-13 14:00', '2018-12-13 15:00');

insert into calendar(text, start_date, end_date)
values ('Michou', '2018-12-15 15:30', '2018-12-15 16:30');
