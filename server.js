//express
var express = require('express');
var app = express();
var http = require('http');
var server = http.createServer(app);
var path = require('path');
var config = require('./public/config.json');
var bodyParser = require("body-parser");

//config
var fs = require('fs');
//var config = JSON.parse(fs.readFileSync('public/config.json', 'utf-8'));

var fs = require('fs');

app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//INDEX CONNEXION/SUBSCRIPTION
app.get('/', function(req, res)
{
  res.sendFile(path.join(__dirname + '/index.html'));
});

app.get('/profile', function(req, res)
{
  res.sendFile(path.join(__dirname + '/profile.html'));
  //console.log(req.query.last_name);
  //res.render('profile', {title: 'POST test',message : 'POST test'});
});

app.get('/admin', function(req,res) {
  res.sendFile(path.join(__dirname + '/admin.html'));
});

app.get('/building', function(req, res)
{
  res.sendFile(path.join(__dirname + '/building.html'));
});

app.get('/graffiti', function(req, res)
{
  res.sendFile(path.join(__dirname + '/graffiti.html'));
});

app.get('/calendar', function(req, res) {
  res.sendFile(path.join(__dirname + '/calendar.html'));
});

app.get('/demande', function(req, res)
{
  res.sendFile(path.join(__dirname + '/demande.html'));
});

app.get('/client', function(req, res)
{
  res.sendFile(path.join(__dirname + '/client.html'));
});

app.post('/', function(req, res)
{
  res.redirect('back');
});

app.get('/init', function(req, res){
  console.log("Get init");
});


app.get('/data', function(req, res){
  var sql = 'SELECT * FROM calendar';
    con.query(sql, function (err, data){
      console.log(data);
      res.send(data);
    });
});


app.post('/data', function(req, res){
  console.log(req.body);
  console.log("Post Data");
});

console.log(config);
//initialize database
var mysql = require('mysql');
var con = mysql.createConnection({
  host: "localhost",
  database: 'nlpfdb',
  user: "root",
  password: config.password
  });

const config2 = {
  username : 'postgres',
  database: 'nlpfdb',
  password : '',
  port: 5432
};

const pg = require('pg');
const pool = new pg.Pool(config2);

//PROFILE
const profile = {
  first_name : '',
  last_name : '',
  email : '',
  tel : ''
};

//socket
var io = require('socket.io').listen(server);
io.sockets.on('connection', function(socket, pseudo)
{
  socket.on('info_profile', function()
  {
    //Profile information
    console.log("IN info_profiles");
    var sql = 'SELECT * FROM profile where status=1';
    con.query(sql, function (err, rows, fields){
      if (err) throw err;
      for (i = 0; i < rows.length; i++)
      {
        console.log(rows[i]);
        socket.emit('info_profile', { email: rows[i].email, first_name: rows[i].first_name, last_name: rows[i].last_name });
      }
    });

    var sql = 'SELECT * FROM profile where status=2';
    con.query(sql, function (err, rows, fields){
      if (err) throw err;
      for (i = 0; i < rows.length; i++)
      {
        console.log(rows[i]);
        socket.emit('info_profile_ban', { email: rows[i].email, first_name: rows[i].first_name, last_name: rows[i].last_name });
      }
    });

  });

  socket.on('info_graffitis', function()
  {
      var sql = 'SELECT * FROM building';
      con.query(sql, function (err, rows, fields){
            if (err) throw err;
           for (i = 0; i < rows.length; i++)
           {
            console.log(rows[i]);
            socket.emit('info_build', { id: rows[i].id, name: rows[i].name, adress: rows[i].adress });
           }

      });

    var sql = 'SELECT * FROM graffiti';
    con.query(sql, function (err, rows, fields){
      if (err) throw err;
      for (i = 0; i < rows.length; i++)
      {
        console.log("yolo : " +  rows[i]);
        var stringStatus = ""
        switch (rows[i].status) {
          case 1:
            stringStatus = "En cours"
            break;
          case 2:
            stringStatus = "Accepté"
            break
          case 3:
            stringStatus = "Refusé"
            break
          default:
            break
          }

          //console.log("Picture: " + rows[i].link_picture);
          socket.emit('info_graffitis', { id: rows[i].id, id_building: rows[i].id_building, floor: rows[i].floor, location: rows[i].location, status: stringStatus, description: rows[i].description, picture: rows[i].link_picture });
        }
    });
  });

  socket.on('info', function()
  {
    //Profile information
    console.log(profile);
    socket.emit('info', { first_name:profile.first_name, last_name:profile.last_name, email:profile.email, tel:profile.tel });

      var sql = 'SELECT * FROM building where id_client=' + mysql.escape(profile.email);
      con.query(sql, function (err, rows, fields){
            if (err) throw err;
           for (i = 0; i < rows.length; i++)
           {
            console.log(rows[i]);
            socket.emit('info_build', { id: rows[i].id, name: rows[i].name, adress: rows[i].adress });
           }

      });

      var sql = 'SELECT graffiti.id as id, building.name as name, graffiti.status as status, graffiti.link_picture as link_picture  from graffiti INNER JOIN building ON building.id=graffiti.id_building where id_client=' + mysql.escape(profile.email);
      con.query(sql, function (err, rows, fields){
            if (err) throw err;
           for (i = 0; i < rows.length; i++)
           {

            socket.emit('info_graffiti', { id: rows[i].id, name: rows[i].name, status: rows[i].status, picture: rows[i].link_picture });
           }
      });
  });



  socket.on('connexion', function(email, password)
  {

    con.connect(function(err)
    {
      if (err) throw err;
      console.log("Connected!");
      var sql = 'SELECT * FROM profile WHERE email = ' + mysql.escape(email);
      con.query(sql, function (err, rows, fields){
            if (err) throw err;
            console.log(rows[0].email);
            profile.first_name = rows[0].first_name;
            profile.last_name = rows[0].last_name;
            profile.email = rows[0].email;
            profile.tel = rows[0].num;
            profile.status = rows[0].status;
            status = rows[0].status;

            if (status == 0){
              socket.emit('admin', {ok: true, message: "Admin bob connected"});
            }

            socket.emit('validate', { ok: true, message: "Connexion successfull" });
            console.log("Connexion successfull : " + rows[0].first_name + rows[0].email);
      });
    });
  });

  //Subscription
  socket.on('subscription', function(email, password, first_name, last_name, adress, num, status)
  {
    con.connect(function(err)
    {
      if (err) throw err;
      console.log("Connected!");
      var sql = 'Insert into profile values (' + mysql.escape(email) + ',' +mysql.escape(password)
      + ',' +mysql.escape(first_name) + ',' +mysql.escape(last_name) + ',' +mysql.escape(adress) + ',' +mysql.escape(num) + ',' +mysql.escape(status) + ');'
      con.query(sql, function (err, rows, fields){
            if (err) throw err;
            profile.first_name = first_name;
            profile.last_name = last_name;
            profile.email = email;
            profile.tel = num;
            socket.emit('validate', { ok: true, message: "User register." });
            console.log("New user : " + first_name + " " + last_name);
      });
    });
  });
//Add to calendar
socket.on('accept_graffiti', function(id, text, date)
  {
      var end_date = date
      console.log("IN accept_graffiti");
      var sql = 'SELECT * FROM calendar';
      con.query(sql, function (err, data){
        for (i=0;i<data.length;i++) {
          var start_date = new Date(data[i].start_date);
          var ended_date = new Date(data[i].end_date);
          var real_date = new Date(date);
          //Gestion de la date sur une tranche d'horaire invalide
          if (start_date > real_date && ended_date > real_date) {
            console.log("Invalide");
            throw err;
          }
        }
      });
      var sql = 'insert into calendar(text, start_date, end_date) values("' + text + '", "' + date + '", "' + end_date + '");'
      con.query(sql, function (err, rows, fields){
            if (err) throw err;
          socket.emit('validate', { ok: true, message: "accept_graffiti" });
          console.log("accept_graffiti : " + text + date);
      });
      var sql = 'update graffiti set status=2, date="' + date + '" where id=' + id + ';'
      con.query(sql, function (err, rows, fields){
            if (err) throw err;
          socket.emit('validate', { ok: true, message: "accept_graffiti" });
          console.log("accept_graffiti : " + text + date);
      });
  });

//Ban Profile
socket.on('ban_profile', function(email)
  {
      console.log("IN ban_profile");
      var sql = 'update profile set status=2 where email="' + email + '";'
      con.query(sql, function (err, rows, fields){
            if (err) throw err;
          socket.emit('validate', { ok: true, message: "Ban profile" });
          console.log("Profile ban : " + email);
      });
  });

 socket.on('deban_profile', function(email)
  {
      console.log("IN ban_profile");
      var sql = 'update profile set status=1 where email="' + email + '";'
      con.query(sql, function (err, rows, fields){
            if (err) throw err;
          socket.emit('validate', { ok: true, message: "Deban profile : " + email});
          console.log("Profile deban : " + email);
      });
  });

//Update graffiti
socket.on('update_graffiti', function(id)
  {
      console.log("IN update_graffiti");
      var sql = 'update graffiti set status=1 where id=' + id + ';'
      con.query(sql, function (err, rows, fields){
            if (err) throw err;
          socket.emit('validate', { ok: true, message: "Update graffiti" });
          console.log("Update graffiti : " + id);
      });
  });

//Ban graffiti
socket.on('ban_graffiti', function(id, description)
  {
      console.log("IN ban_graffiti, description : " + description + " .");

      var sql = 'update graffiti set status=3, description="' + description + '" where id=' + id + ';'
      con.query(sql, function (err, rows, fields){
            if (err) throw err;
          socket.emit('validate', { ok: true, message: "Ban graffiti" });
          console.log("Ban graffiti : " + description);
      });
  });

  //New building
  socket.on('new_building', function(name, adress)
  {
      var sql = 'Insert into building(id_client, name, adress) values (' + mysql.escape(profile.email) + ',' +mysql.escape(name) + ',' +mysql.escape(adress) + ');'
      con.query(sql, function (err, rows, fields){
            if (err) throw err;
          socket.emit('validate', { ok: true, message: "Build added" });
          console.log("New building : " + name);
      });
  });

  //New graffiti
  socket.on('new_graffiti', function(id_building, location, floor, picture)
  {
      console.log(id_building, location, floor, picture);
      var sql = 'Insert into graffiti(id_building, location, floor, status, date, description, link_picture) values (' + mysql.escape(id_building)
      + ',' +mysql.escape(location) + ',' +mysql.escape(floor) + ', 1, null, null,' + mysql.escape(picture) + ');'
      con.query(sql, function (err, rows, fields){
            if (err) throw err;
          socket.emit('validate', { ok: true, message: "Graffiti added" });
      });
  });

});

const multer = require("multer");

const handleError = (err, res) => {
  res
    .status(500)
    .contentType("text/plain")
    .end("Oops! Something went wrong!");
};

const upload = multer({
dest: "./public/picture/"
// you might also want to set some limits: https://github.com/expressjs/multer#limits
});

app.post(
    "/upload",
    upload.single("file" /* name attribute of <file> element in your form */),
    (req, res) => {
    const tempPath = req.file.path.name;
    const targetPath = path.join("./public/picture/image.png");

    if (path.extname(req.file.originalname).toLowerCase() === ".png") {
    fs.rename(tempPath, targetPath, err => {
        if (err) return handleError(err, res);

        res.sendFile(path.join(__dirname + '/profile.html'));
        });
    } else {
    fs.unlink(tempPath, err => {
        if (err) return handleError(err, res);

        res
        .status(403)
        .contentType("text/plain")
        .end("Only .png files are allowed!");
        });
    }
    }
);

server.listen(config.port);
